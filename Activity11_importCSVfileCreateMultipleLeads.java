package ProjectSuiteCRM;

//Objective/Goal - Create multiple leads by importing a CSV file

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity11_importCSVfileCreateMultipleLeads {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 25);

	File file = new File("src/ProjectSuiteCRM/Leads.csv");


	WebElement salesLeads, Leads, uploadFile;

	List<WebElement> importedLeads;
	String name = "Mr. Giovanni Santoro";
	boolean flag=false;


	@Test
	public void f() {

		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		System.out.println("Start of execution");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='grouptab_0']"))));

		//Click on Sales menu
		driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]")).click();
		System.out.println("Clicked on Sales ");
		//Wait for Leads under Sales menu to appear and click on it
		salesLeads = driver.findElement(By.xpath("//*[@id='moduleTab_9_Leads']"));
		wait.until(ExpectedConditions.elementToBeClickable(salesLeads));
		salesLeads.click();
		System.out.println("Clicked on Sales -> Leads ");
		//Wait for the text LEADS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to Sales -> Leads ");
		Leads = driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"));
		System.out.println("In the " + Leads.getText() + " page ");

		//Wait until Import Leads element appears and is clickable
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[@data-action-name='Import']/div[@class='actionmenulink']"))));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@data-action-name='Import']/div[@class='actionmenulink']"))));

		//Click on Import Leads

		driver.findElement(By.xpath("//a[@data-action-name='Import']/div[@class='actionmenulink']")).click();
		System.out.println("Clicked on Import Leads");
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));
		System.out.println("Inside Import Leads page ");
		System.out.println("Performing " + driver.findElement(By.xpath("//h2[@class='module-title-text']")).getText());
		//Clicking on the Browse button
		System.out.println("The absolute path of the file is : " + file.getAbsolutePath());
		//uploadFile = driver.findElement(By.xpath("//input[@id='userfile' and @name='userfile']"));
		uploadFile = driver.findElement(By.xpath("//input[@id='userfile']"));
		uploadFile.sendKeys(file.getAbsolutePath());
		driver.findElement(By.xpath("//*[@id='gonext']")).click();
		System.out.println("Clicked on 1st Next Button");

		driver.findElement(By.xpath("//*[@id='gonext']")).click();
		System.out.println("Clicked on 2nd Next Button");

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='gonext']"))));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='gonext']"))));
		driver.findElement(By.xpath("//*[@id='gonext']")).click();
		System.out.println("Clicked on 3rd Next Button");

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='importnow']"))));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='importnow']"))));
		driver.findElement(By.xpath("//*[@id='importnow']")).click();
		System.out.println("Clicked on Import Now Button");

		System.out.println("In the page "+driver.findElement(By.xpath("//h2[@class='module-title-text']")).getText());

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@title='FinishedLeads']"))));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@title='FinishedLeads']"))));

		//commenting the below lines even without clicking on exit, the import is completed
		//Hence, moving directly to View Leads page. The following lines can be uncommented and used if desired
		//driver.findElement(By.xpath("//input[@title='FinishedLeads']")).click();
		//System.out.println("Clicked on Exit/Finish Button");
		//System.out.println("In the page "+driver.findElement(By.xpath("//h2[@class='module-title-text']")).getText());
		driver.findElement(By.xpath("//a[@data-action-name='List']/div[@class='actionmenulink']")).click();

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("In the " + driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]")).getText() + " page ");
		System.out.println("============================================================================");
		importedLeads = driver.findElements(By.xpath("//tr[contains(@class,'ListRowS1')]/td[@field='name']"));
		System.out.println("The names in the rows are:");
		System.out.println("============================================================================");
		for(WebElement leadsName:importedLeads) {
			System.out.println(leadsName.getText());
			if(leadsName.getText().equalsIgnoreCase(name)) {
				Assert.assertTrue(leadsName.getText().equalsIgnoreCase(name));
				flag=true;
			}

		}
		System.out.println("============================================================================");
		System.out.println("____________________________________________________________________________");
		if(flag) {
			System.out.println(name+" exist in the Leads table");
			Reporter.log(name+" exist in the Leads table");
		}
		else {
			System.out.println(name+" DO NOT exist in the Leads table");
			Assert.assertTrue(flag);
			Reporter.log(name+" DO NOT exist in the Leads table");
		}
		System.out.println("----------------------------------------------------------------------------");
		System.out.println("============================================================================");
	}
	@BeforeClass
	public void beforeClass() {

		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();


	}

	@AfterClass
	public void afterClass() {

		System.out.println("Closing the Browser");
		driver.close();

	}

}
