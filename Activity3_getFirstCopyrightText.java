package ProjectSuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity3_getFirstCopyrightText {


	//Creating and instantiating WebDriver
	WebDriver driver = new FirefoxDriver();
	//Creating the WebElement object copyrightText
	WebElement copyrightText;


	@Test
	public void f() {

		//Instantiating copyrightText to point to the first copyright text in the footer to the console
		copyrightText=driver.findElement(By.xpath("//*[@id=\"admin_options\"]"));

		//Printing the first copyright text in the footer to the console
		System.out.println("The url of the header image is : " + copyrightText.getText());

	}
	@BeforeClass
	public void beforeClass() {

		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");

	}

	@AfterClass
	public void afterClass() {

		//closing the browser
		driver.close();

	}

}
