package ProjectSuiteCRM;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_loginAndVerify {

	//Creating and instantiating WebDriver
	WebDriver driver = new FirefoxDriver();
	//Creating the WebElement object copyrightText
	WebElement userName,password,homeButton;
	String initialURL,currentUrl,homeButtonUrl;

	@Test
	public void f() {
		initialURL=driver.getCurrentUrl();
		System.out.println("The initial URL of the webpage is : " + initialURL);
		userName=driver.findElement(By.xpath("//*[@id=\"user_name\"]"));
		password=driver.findElement(By.xpath("//*[@id=\"username_password\"]"));
		userName.sendKeys("admin");
		password.sendKeys("pa$$w0rd");
		//Clicking the "LOG IN" button to log into the website
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();

		homeButton=driver.findElement(By.xpath("//a[contains(@class,'navbar-brand with-home-icon suitepicon suitepicon-action-home')]"));
		currentUrl=driver.getCurrentUrl();

		System.out.println("The current URL of the webpage is : " + currentUrl);
		homeButtonUrl=homeButton.getAttribute("href");
		System.out.println("The URL of the Home Button is : " + homeButtonUrl);

		//Checking whether we have reached the home page by
		//comparing the url of the current page with the url that the Home icon points to when clicked

		Assert.assertEquals(currentUrl, homeButtonUrl);

		if(homeButtonUrl.equals(currentUrl)) {
			System.out.println("We have reached the home page");
		}
		else {
			System.out.println("Home page not reached");
		}
	}
	@BeforeClass
	public void beforeClass() {

		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");

	}

	@AfterClass
	public void afterClass() {

		//closing the browser
		driver.close();

	}

}
