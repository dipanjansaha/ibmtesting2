package ProjectSuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Activity2_getUrlHeaderImage {

	//Creating and instantiating WebDriver
	WebDriver driver = new FirefoxDriver();

	//Creating the objects for the WebDriverWait, WebElement for the image
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement image;

	@Test
	public void f() {

		//instantiating and pointing the WebElement Image created earlier to be worked upon the header image
		//Either of the below two lines of code would work
		//image= driver.findElement(By.xpath("//form[1]/div[1]/img"));
		image = driver.findElement(By.xpath("//form[contains(@class , 'form-signin')]/div[contains(@class,'companylogo')]/img"));
		//Getting the attribute of 'src' in the img tag (pointed by the image WebElement) and printing it
		System.out.println("The url of the header image is : " + image.getAttribute("src"));


	}
	@BeforeClass
	public void beforeClass() {
		//Maximizing the browser
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");


	}

	@AfterClass
	public void afterClass() {
		//closing the browser
		driver.close();
	}

}
