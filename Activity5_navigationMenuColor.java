package ProjectSuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity5_navigationMenuColor {

	//Creating WebDriver and WebElement objects and instantiating the WebDriver object
	WebDriver driver = new FirefoxDriver();
	WebElement toolBar;

	@Test
	public void f() {

		System.out.println("Logged into the website");
		toolBar = driver.findElement(By.xpath("//div[contains(@id,'toolbar')]/ul"));
		//Printing the Color of the toolbar
		System.out.println("Color of the navigation menu is : " + toolBar.getCssValue("color").toString());

	}
	@BeforeClass
	public void beforeClass() {

		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();

	}

	@AfterClass
	public void afterClass() {

		//closing the browser
		driver.close();

	}

}
