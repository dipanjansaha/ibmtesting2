package ProjectSuiteCRM;

//Objective or Goal - Open the Leads page and print the first 10 values in the Name column and the User column of the table 

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity9_userNames_fullNamesFromLeadsTable {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 20);

	int counter=1;

	List<WebElement> rows;

	WebElement salesLeads, Leads;

	@Test
	public void f() {

		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		System.out.println("Start of execution");
		Reporter.log("======================================================================================================================================");
		Reporter.log("Activity 9: Objective or Goal - Open the Leads page and print the first 10 values in the Name column and the User column of the table ");
		Reporter.log("======================================================================================================================================");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='grouptab_0']"))));

		//Click on Sales menu
		driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]")).click();

		//Wait for Leads under Sales menu to appear and click on it
		salesLeads = driver.findElement(By.xpath("//*[@id='moduleTab_9_Leads']"));
		wait.until(ExpectedConditions.elementToBeClickable(salesLeads));
		salesLeads.click();
		System.out.println("Clicking on Sales -> Leads ");
		//Wait for the text LEADS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to Sales -> Leads ");
		Leads = driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"));
		System.out.println("In the " + Leads.getText() + " page ");

		rows = driver.findElements(By.xpath("//table[contains(@class,'list view table-responsive')]/tbody/tr[contains(@class,'ListRowS1')]/td[@field='name' or @field='assigned_user_name']"));
		System.out.println("Total no. of rows in the table : " +(rows.size())/2);
		Reporter.log("Total no. of rows in the table : " +(rows.size())/2);
		Reporter.log("----------------------------------------------------------------");
		//rows = driver.findElements(By.xpath("//table[contains(@class,'list view table-responsive')]/tbody/tr[contains(@class,'ListRowS1')]"));
		System.out.println("---> Printing the Names and Assigned Usernames : ");
		Reporter.log("Printing the Names and Assigned Usernames : ");
		Reporter.log("----------------------------------------------------------------");
		for(WebElement rowItems: rows) {
			if ((counter%2)!=0) {
				System.out.println("----------------------------------------------------------------");
				System.out.println("Name and Assigned Username in row "+ ((counter/2)+1) + " are: ");
				Reporter.log("----------------------------------------------------------------");
				Reporter.log(" Name and Assigned Username in row " + ((counter/2)+1) + " are: ");
			}
			System.out.println(rowItems.getText());

			Assert.assertTrue(true, rowItems.getText());
			Reporter.log(rowItems.getText() + " | ");
			//Using an if with break, followed by a counter, to print only the first 10 Names and Assigned Usernames only
			//To be removed if all the names are to be printed 
			if(counter>=10) {
				break;
			}
			counter++;
		}
		System.out.println("----------------------------------------------------------------");
		Reporter.log("----------------------------------------------------------------");
	}
	@BeforeClass
	public void beforeClass() {

		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();

	}

	@AfterClass
	public void afterClass() {
		System.out.println("Closing the Browser");
		Reporter.log("======================================================================================================================================");
		Reporter.log("-------------------------------------- End of Report ---------------------------------------------------------------------------------");
		Reporter.log("======================================================================================================================================");
		driver.close();
	}

}
