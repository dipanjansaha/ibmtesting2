package ProjectSuiteCRM;
// Objective or Goal - Open the accounts page and print the names of the first 5 odd-numbered rows of the table to the console.

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity8_first5OddNumberedRows {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 20);
	int counter=1;

	List<WebElement> rows;

	WebElement salesAccounts, Accounts;

	@Test
	public void f() {

		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		System.out.println("Start of execution");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='grouptab_0']"))));

		//Click on Sales menu
		driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]")).click();

		//Wait for Leads under Sales menu to appear and click on it
		salesAccounts = driver.findElement(By.xpath("//*[@id='moduleTab_9_Accounts']"));
		wait.until(ExpectedConditions.elementToBeClickable(salesAccounts));
		salesAccounts.click();
		System.out.println("Clicking on Sales -> Accounts ");
		//Wait for the text ACCOUNTS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to Sales -> Accounts ");
		Accounts = driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"));
		System.out.println("In the " + Accounts.getText() + " page ");

		//rows = driver.findElements(By.xpath("//table[contains(@class,'list view table-responsive')]/tbody/tr[contains(@class,'oddListRowS1')]/td[contains(@class, 'inlineEdit')][contains(@field, 'name')][contains(@type, 'name')]"));
		//The above line also works. It uses a combination of/multiple "contains(@ , '')" in tr tag to get the particular element, i.e., name in this case

		rows = driver.findElements(By.xpath("//table[contains(@class,'list view table-responsive')]/tbody/tr[contains(@class,'oddListRowS1')]/td[@field = 'name']"));
		System.out.println("---> Printing the odd row elements : ");
		for(WebElement rowItems: rows) {

			System.out.println("Name in row " +counter+ " is : " + rowItems.getText());
			Assert.assertTrue(true, "Name in row " +counter+ " is : " + rowItems.getText());
			Reporter.log(" Name in row " +counter+ " is : " + rowItems.getText() + " | ");
			//Using an if with break, followed by a counter, to print only the first 5 names only
			//To be removed if all the names are to be printed 
			if(counter>=5) {
				break;
			}
			counter++;
		}
	}

	@BeforeClass
	public void beforeClass() {
		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Closing the Browser");
		driver.close();
	}

}
