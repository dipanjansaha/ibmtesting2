package ProjectSuiteCRM;

//Objective/Goal - To Create Product by using an external Excel to add products


import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity13_createProductUsingExternalExcel {



	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebElement products, tempName;
	List<WebElement> productNames;

	String filePath = "src/ProjectSuiteCRM/product.xlsx";
	String fileName = "product.xlsx";
	String sheetName = "Product Details";
	String productName;

	boolean flag;


	@Test
	public void Step1_createProduct() {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

		System.out.println("Start of execution");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@id='grouptab_5']"))));

		//Click on ALL menu
		driver.findElement(By.xpath("//a[@id='grouptab_5']")).click();
		System.out.println("Clicked on ALL ");

		//products = driver.findElement(By.xpath("//ul[@class='nav navbar-nav']/li[@class='topnav all']/span[@class='notCurrentTab']/ul[@class='dropdown-menu']/li[25]/a"));
		products = driver.findElement(By.xpath("(//a[contains(.,'Products')])[3]"));

		wait.until(ExpectedConditions.elementToBeClickable(products));
		products.click();

		System.out.println("Clicked on ALL -> Products ");

		//Wait for the text PRODUCTS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to " + driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]")).getText() + " page ");
		//Wait until Create Product is clickable and then click
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='actionmenulink'][contains(.,'Create Product')]"))));
		driver.findElement(By.xpath("//div[@class='actionmenulink'][contains(.,'Create Product')]")).click();

	}

	@Test
	public void Step2_fillProductInfo() {
		//putting the entire code under try-catch block to avoid using the same multiple times in the function
		try {

			System.out.println("Creating Products with feedback/data from external excel file");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));


			// File file =    new File(filePath+"\\"+fileName);
			File file = new File(filePath);
			//Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);
			Workbook myWorkbook = null;
			//Find the file extension by splitting file name in substring  and getting only extension name
			String fileExtensionName = fileName.substring(fileName.indexOf("."));
			//Check condition if the file is xlsx file
			if(fileExtensionName.equals(".xlsx")){
				//If it is xlsx file then create object of XSSFWorkbook class
				myWorkbook = new XSSFWorkbook(inputStream);
			}
			//Check condition if the file is xls file
			else if(fileExtensionName.equals(".xls")){
				//If it is xls file then create object of HSSFWorkbook class
				myWorkbook = new HSSFWorkbook(inputStream);
			}

			//Read sheet inside the workbook by its name
			//Sheet mySheet = myWorkbook.getSheetAt(0);
			Sheet mySheet = myWorkbook.getSheet(sheetName);
			//Find number of rows in excel file
			int rowCount = mySheet.getLastRowNum()-mySheet.getFirstRowNum();

			//Create a loop over all the rows of excel file to read it
			//for (int i = 0; i < rowCount+1; i++) {
			//The above line starts from the 1st row of the excel, row 0
			//However, To start from the 2nd row, in order to avoid the header row, putting i=1 as below
			for (int i = 1; i < rowCount+1; i++) {
				Row row = mySheet.getRow(i);
				//Create a loop to print cell values in a row
				for (int j = 0; j < row.getLastCellNum(); j++) {
					//To Print Excel data in console, we can use either of the the following two lines
					//This is if we are sure that all the cell data are of type String
					//System.out.print(row.getCell(j).getStringCellValue().toString()+"|| ");
					//This is if we are NOT sure that all the cell data are of type String
					//System.out.print(row.getCell(j).toString()+"|| ");

					//using switch to put appropriate cell value to the appropriate field
					switch(j) {

					case 0:
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='name']"))));
						driver.findElement(By.xpath("//input[@name='name']")).click();
						driver.findElement(By.xpath("//input[@name='name']")).clear();
						driver.findElement(By.xpath("//input[@name='name']")).sendKeys(row.getCell(j).toString());
						productName = row.getCell(j).toString();
						break;

					case 1:
						driver.findElement(By.xpath("//input[@name='price']")).click();
						driver.findElement(By.xpath("//input[@name='price']")).clear();
						driver.findElement(By.xpath("//input[@name='price']")).sendKeys(row.getCell(j).toString());	
						break;

					case 2:
						driver.findElement(By.xpath("//input[@name='cost']")).click();
						driver.findElement(By.xpath("//input[@name='cost']")).clear();
						driver.findElement(By.xpath("//input[@name='cost']")).sendKeys(row.getCell(j).toString());	
						break;

					case 3:

						driver.findElement(By.xpath("//input[@type='text'][@id='part_number']")).click();
						driver.findElement(By.xpath("//input[@type='text'][@id='part_number']")).clear();
						driver.findElement(By.xpath("//input[@type='text'][@id='part_number']")).sendKeys(row.getCell(j).toString());

						break;

					case 4:
						driver.findElement(By.xpath("//textarea[@name='description']")).click();
						driver.findElement(By.xpath("//textarea[@name='description']")).clear();
						driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys(row.getCell(j).toString());
						break;

					default:
						System.out.println("Were there more values in the Excel??? Please insert another Case in the code, and then add the right values to the right fields");
						break;

					}
				}
				//System.out.println();
			} 

			//Click on Save
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("(//input[@id='SAVE'])[2]"))));
			driver.findElement(By.xpath("(//input[@id='SAVE'])[2]")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));
			tempName = driver.findElement(By.xpath("//span[@class='sugar_field'][@id='name']"));

			Assert.assertTrue(tempName.getText()+" is created ", tempName.getText().equalsIgnoreCase(productName));
			Assert.assertEquals("Is "+productName+" created? ", true, tempName.getText().equalsIgnoreCase(productName));
			Reporter.log("Is product "+tempName.getText()+" created? Ans: "+tempName.getText().equalsIgnoreCase(productName));


		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void Step3_viewCreatedProduct() {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='actionmenulink'][contains(.,'View Products')]"))));
		driver.findElement(By.xpath("//div[@class='actionmenulink'][contains(.,'View Products')]")).click();

		//Either of the following 2 lines work
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text'][contains(.,'Products')]"))));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//table[@class='list view table-responsive']"))));

		productNames = driver.findElements(By.xpath("//tr[contains(@class,'ListRowS1')]/td[@field='name']"));
		for(WebElement pName:productNames) {
			if(pName.getText().toString().equalsIgnoreCase(productName)) {
				System.out.println("Product with Product Name: "+productName+" has been created");
				Assert.assertTrue("Product with Product Name: "+productName+" has been created", pName.getText().toString().equalsIgnoreCase(productName));
				Reporter.log("Product with Product Name: "+productName+" has been created");
				flag=true;
				break;
			}

			System.out.println("Test Line");

		}


		if(flag==false) {
			System.out.println("Meeting with subject line: "+productName+" has NOT been created");
			Assert.assertTrue("Meeting with subject line: "+productName+" has NOT been created", flag);
			Reporter.log("Meeting with subject line: "+productName+" has NOT been created");

		}
	}

	@BeforeClass
	public void beforeClass() {
		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@type='password'][contains(@id,'password')]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[contains(@type,'submit')]")).click();
	}

	@AfterClass
	public void afterClass() {

		System.out.println("Closing the Browser");
		driver.close();
	}

}
