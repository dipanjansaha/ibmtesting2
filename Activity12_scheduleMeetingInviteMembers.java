package ProjectSuiteCRM;
//Objective/Goal - To schedule a meeting and include at least 3 invitees

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity12_scheduleMeetingInviteMembers {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 25);

	List<WebElement> meetingSubjects;


	String meetingSubject = "Test Subject 1 - Dipanjan";
	String meetingDescription = "The Objective/Goal - To schedule a meeting and include at least 3 invitees";
	String[] invitee = new String[] { "max", "chris", "sarah"};

	int counter;
	boolean flag=false;

	@Test
	public void f() {

		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		System.out.println("Start of execution");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='grouptab_3']"))));

		//Click on Activities menu
		driver.findElement(By.xpath("//*[@id='grouptab_3']")).click();
		System.out.println("Clicked on Activities ");

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='moduleTab_9_Meetings']"))));
		driver.findElement(By.xpath("//*[@id='moduleTab_9_Meetings']")).click();

		System.out.println("Clicked on Activities -> Meetings ");

		//Wait for the text MEETINGS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to " + driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]")).getText() + " page ");

		//Wait until Schedule Meeting element appears and is clickable
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[@data-action-name='Schedule_Meeting']/div[@class='actionmenulink']"))));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@data-action-name='Schedule_Meeting']/div[@class='actionmenulink']"))));

		//Click on Schedule Meeting

		driver.findElement(By.xpath("//a[@data-action-name='Schedule_Meeting']/div[@class='actionmenulink']")).click();

		//Wait for the text CREATE to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		System.out.println("Navigated to " + driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]")).getText() + " page ");

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='name']"))));
		driver.findElement(By.xpath("//*[@id='name']")).click();
		driver.findElement(By.xpath("//*[@id='name']")).sendKeys(meetingSubject);

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='description']"))));
		driver.findElement(By.xpath("//*[@id='description']")).click();
		driver.findElement(By.xpath("//*[@id='description']")).sendKeys(meetingDescription);

		for(counter=0; counter<3; counter++) {

			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("search_first_name"))));
			driver.findElement(By.id("search_first_name")).click();
			driver.findElement(By.id("search_first_name")).sendKeys(invitee[counter]);

			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("invitees_search"))));
			driver.findElement(By.id("invitees_search")).click();
			driver.findElement(By.id("invitees_search")).sendKeys(invitee[counter]);
			System.out.println("Searching for invitee: "+invitee[counter]);
			/*
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@id='invitees_add_1']"))));
			driver.findElement(By.xpath("//*[@id='invitees_add_1']")).click();
			 */


			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("search_first_name"))));
			//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("invitees_add_1"))));
			driver.findElement(By.id("invitees_add_1")).click();
			System.out.println("Added invitee: "+invitee[counter]);

			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("search_first_name"))));
			//driver.findElement(By.id("search_first_name")).click();
			driver.findElement(By.id("search_first_name")).clear();

			//driver.navigate().refresh();
		}


		/*
		//To add me as a contact
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='create_invitee_as_contact']"))));
		driver.findElement(By.xpath("//*[@id='create_invitee_as_contact']")).click();

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[1]/td[2]/input[@name='first_name']"))));
		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[1]/td[2]/input[@name='first_name']")).click();
		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[1]/td[2]/input[@name='first_name']")).sendKeys("Dipanjan");

		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[2]/td[2]/input[@name='last_name']")).click();
		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[2]/td[2]/input[@name='last_name']")).sendKeys("Saha");

		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[3]/td[2]/input[@name='email1']")).click();
		driver.findElement(By.xpath("//table[@class='edit view']/tbody/tr[3]/td[2]/input[@name='email1']")).sendKeys("mail@nomail.com");

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='create-invitee-btn']"))));
		driver.findElement(By.xpath("//*[@id='create-invitee-btn']")).click();
		 */

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='save_and_send_invites_header']"))));
		driver.findElement(By.xpath("//*[@id='SAVE_HEADER']")).click();
		System.out.println("Saving the meeting invite ");
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[@class='module-title-text']"))));
		System.out.println("Meeting with subject " + driver.findElement(By.xpath("//h2[@class='module-title-text']")).getText() + " is saved ");

		driver.findElement(By.xpath("//a[@data-action-name='List']/div[@class='actionmenulink']")).click();

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//table[@class='list view table-responsive']"))));
		meetingSubjects = driver.findElements(By.xpath("//tr[contains(@class,'ListRowS1')]/td[@field='name']"));
		for(WebElement meetingSub:meetingSubjects) {
			if(meetingSub.getText().toString().equalsIgnoreCase(meetingSubject)) {
				System.out.println("Meeting with subject line: "+meetingSubject+" has been created");
				Assert.assertTrue(meetingSub.getText().toString().equalsIgnoreCase(meetingSubject), "Meeting with subject line: "+meetingSubject+" has been created");
				Reporter.log("Meeting with subject line: "+meetingSubject+" has been created");
				flag=true;
				break;
			}

			System.out.println("Test Line");

		}


		if(flag==false) {
			System.out.println("Meeting with subject line: "+meetingSubject+" has NOT been created");
			Assert.assertTrue(flag, "Meeting with subject line: "+meetingSubject+" has NOT been created");
			Reporter.log("Meeting with subject line: "+meetingSubject+" has NOT been created");

		}
	}
	@BeforeClass
	public void beforeClass() {

		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();

	}

	@AfterClass
	public void afterClass() {

		System.out.println("Closing the Browser");
		driver.close();

	}

}
