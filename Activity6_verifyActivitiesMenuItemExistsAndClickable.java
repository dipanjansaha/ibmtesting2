package ProjectSuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity6_verifyActivitiesMenuItemExistsAndClickable {

	//Creating WebDriver and WebElement objects and instantiating the WebDriver object
	WebDriver driver = new FirefoxDriver();

	List<WebElement> menuItems, toolBar;
	String menuElement= "Activities";
	Boolean state=false;

	@Test
	public void f() {
		//Printing the menu items
		//toolBar = driver.findElements(By.xpath("//div[contains(@id,'toolbar')]/ul"));
		toolBar = driver.findElements(By.xpath("//div[contains(@id,'toolbar')]/ul/li"));
		System.out.println("Printing all the Menu Items/Elements: ");
		System.out.println("==================================");
		for(WebElement items : toolBar) {
			System.out.println(items.getText());
		}
		System.out.println("==================================");

		//Ensuring that the “Activities” menu item exists, displayed and enabled/clickable
		menuItems = driver.findElements(By.xpath("//div[contains(@id,'toolbar')]/ul//a"));
		for(WebElement menuItem : menuItems) {
			if(menuItem.getText().equalsIgnoreCase(menuElement)) {
				state=true;
				System.out.println("The menu item "+ menuItem.getText()+" exists");

				if(menuItem.isDisplayed()) {
					System.out.println("The menu item "+ menuItem.getText()+" is displayed");
					Assert.assertTrue(menuItem.getText().equalsIgnoreCase(menuElement), "The menu item "+ menuItem.getText()+" is displayed");
				}
				else {
					System.out.println("The menu item "+ menuItem.getText()+" is NOT displayed");
					Assert.assertFalse(menuItem.getText().equalsIgnoreCase(menuElement), "The menu item "+ menuItem.getText()+" is NOT displayed");
				}

				if(menuItem.isEnabled()) {
					System.out.println("The menu item "+ menuItem.getText()+" is enabled");
					Assert.assertTrue(menuItem.getText().equalsIgnoreCase(menuElement), "The menu item "+ menuItem.getText()+" is enabled");
				}
				else {
					System.out.println("The menu item "+ menuItem.getText()+" is NOT enabled");
					Assert.assertFalse(menuItem.getText().equalsIgnoreCase(menuElement), "The menu item "+ menuItem.getText()+" is NOT enabled");
				}


			}

		}

		if(state!=true) {
			System.out.println("The menu item "+menuElement+" DO NOT EXIST");
			Assert.assertTrue(state, "The menu item "+menuElement+" DO NOT EXIST");
		}

	}
	@BeforeClass
	public void beforeClass() {

		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Closing the Browser");
		driver.close();
	}

}
