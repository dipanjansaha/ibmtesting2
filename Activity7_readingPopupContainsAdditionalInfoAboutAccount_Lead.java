package ProjectSuiteCRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity7_readingPopupContainsAdditionalInfoAboutAccount_Lead {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 20);

	WebElement salesLeads, additionalInfo, Leads;


	@Test
	public void f() {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		System.out.println("Start of execution");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"))));

		//Click on Sales menu
		driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]")).click();

		//Wait for Leads under Sales menu to appear and click on it
		salesLeads = driver.findElement(By.xpath("//*[@id='moduleTab_9_Leads']"));
		wait.until(ExpectedConditions.elementToBeClickable(salesLeads));
		salesLeads.click();
		System.out.println("Navigating to Sales -> Leads ");
		//Wait for the text LEADS to appear in the page

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"))));
		Leads = driver.findElement(By.xpath("//h2[contains(@class, 'module-title-text')]"));
		System.out.println("In the Leads Page : " + Leads.getText());


		//Click on "Additional information" icon at the end of the row of your newly created lead
		additionalInfo = driver.findElement(By.xpath("//table[contains(@class,'list view table-responsive')]/tbody/tr[1]/td[10]/span"));
		additionalInfo.click();
		System.out.println("Clicked on Additional Info");

		//Getting data for Phone, and printing the same
		System.out.println("The Mobile number is : " + driver.findElement(By.xpath("//span[@class='phone']")).getText());

		Assert.assertTrue(true, "The Mobile number is : " + driver.findElement(By.xpath("//span[@class='phone']")).getText());

	}
	@BeforeClass
	public void beforeClass() {
		//maximizing the browser window
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Closing the Browser");
		driver.close();
	}

}
