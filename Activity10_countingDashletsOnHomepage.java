package ProjectSuiteCRM;

//Objective or Goal - Open the homepage and count the number of the dashlets on the page. Print the number and title of each Dashlet

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity10_countingDashletsOnHomepage {

	//Creating WebDriver, WebDriverWait and WebElement objects and instantiating the WebDriver and WebDriverWait object
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 20);

	List<WebElement> dashlets;

	@Test
	public void f() {

		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		System.out.println("Start of execution");

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//td[@class='dashlet-title']"))));

		dashlets = driver.findElements(By.xpath("//td[@class='dashlet-title']/h3/span[2]"));
		//dashlets = driver.findElements(By.xpath("//td[@class='dashlet-title']//span[2]"));
		//dashlets = driver.findElements(By.xpath("//td[@class='dashlet-title']"));
		//Any of the above three lines would do in this case

		System.out.println("The number of Dashlets are : " + dashlets.size());
		Reporter.log("The number of Dashlets are : " + dashlets.size() + " -->");
		System.out.println("=============================");
		System.out.println("The Titles of Dashlets are : ");
		System.out.println("=============================");
		for(WebElement dashlet: dashlets) {
			System.out.println(dashlet.getText());
			Reporter.log("| " + dashlet.getText() + " |");
		}
		System.out.println("=============================");
	}
	@BeforeClass
	public void beforeClass() {

		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
		//log into the website
		driver.findElement(By.xpath("//*[@id=\"user_name\"]")).sendKeys("admin");
		driver.findElement(By.xpath("//*[@id=\"username_password\"]")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//*[@id=\"bigbutton\"]")).click();

	}

	@AfterClass
	public void afterClass() {
		System.out.println("Closing the Browser");
		driver.close();
	}

}


