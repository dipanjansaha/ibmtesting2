package ProjectSuiteCRM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity1_verifyWebsiteTitle {
	WebDriver driver = new FirefoxDriver();;
	WebDriverWait wait = new WebDriverWait(driver, 10);
	@Test
	public void f() {


		System.out.println("The tile of the page : "+ driver.getTitle());
		Assert.assertEquals(driver.getTitle(), "SuiteCRM");
		driver.close();  
	}
	@BeforeClass
	public void beforeClass() {
		//Maximizing the browser
		driver.manage().window().maximize();
		//Going to the website http://alchemy.hguy.co/crm
		driver.get("http://alchemy.hguy.co/crm");
	}

	@AfterClass
	public void afterClass() {
		//Closing the browser
		driver.close();  
	}

}
